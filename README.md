# [A flutter weather app](https://gitlab.com/getabujob/a-flutter-weather-app)

A weather application to display the current weather at the user’s location and a 5-day forecast.

### [Download APK here](https://gitlab.com/getabujob/a-flutter-weather-app/-/blob/main/build/app/outputs/flutter-apk/app-release.apk)  

### Prerequisites
You must have fluttter sdk already set up on your machine. If not follow [this](https://docs.flutter.dev/get-started/install) instructions to set it up

### Architecture
The app has been split into multiple sections (folders) in order to maintain explicit dependencies for each package with clear boundaries thet enforce the [single responsibility principle](https://en.wikipedia.org/wiki/Single-responsibility_principle).

### App structure
  ```
├── lib
│   ├── components
│   ├── core
│   └── features
│   └── shared
└── test

### UI/UX
The base for the ui is ```Material design by google``` and its standards. You can access its documentation from [material.io](https://material.io/develop/android)

### Common Libraries

Some of the libraries  used:

- [Google Fonts](https://pub.dev/packages/google_fonts)
- [GetIt](https://pub.dev/packages/get_it)
- [Flutter Bloc](https://pub.dev/packages/flutter_bloc)
- [Easy_localization](https://pub.dev/packages/easy_localization)
- [Dio](https://pub.dev/packages/dio)
- [Flutter SVG](https://pub.dev/packages/flutter_svg)
- [Flutter Toast](https://pub.dev/packages/fluttertoast)
- [Fimber](https://pub.dev/packages/fimber)


