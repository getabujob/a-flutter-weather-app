import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:weather_app/core/bloc/currentWeatherCubit/current_weather_state.dart';
import 'package:weather_app/core/data/error_response_model.dart';
import '../../controller/current_city_weather_condition_controller.dart';
import 'data/current_city_params.dart';
import 'data/current_weather_condition_response_model.dart';

class CurrentCityWeatherCubit extends Cubit<CurrentWeatherState> { 
  final CurrentCityWeatherController currentcityWeatherController;

  CurrentCityWeatherCubit({
    required this.currentcityWeatherController,
  }) : super(const CurrentWeatherState.init());
 
      getCityStatements(WeatherCityParams weatherCityParams) async {
    try {
      final Either<ErrorsModel, WeatherInfoModel> eitherResponse =
          await currentcityWeatherController(weatherCityParams);

      emit(
        eitherResponse.fold(
          (l) {
            return CurrentWeatherState.error(l.message!);
          },
          (r) {
            return CurrentWeatherState.success(r);
          },
        ),
      );
    } catch (e) {
      emit(CurrentWeatherState.error(e.toString()));
    }
  }
}
