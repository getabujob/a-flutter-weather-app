import 'package:dartz/dartz.dart'; 
import 'package:weather_app/core/bloc/currentWeatherCubit/data/current_weather_condition_response_model.dart';

import 'package:weather_app/core/data/error_response_model.dart';
import 'package:weather_app/core/repository/weather_repository.dart';
import 'package:weather_app/shared/services/controller_usecase_service.dart';

import '../bloc/currentWeatherCubit/data/current_city_params.dart';

class CurrentCityWeatherController
    extends UseCase<WeatherInfoModel, WeatherCityParams> {
  final WeatherRepository _weatherCityRepository;

  CurrentCityWeatherController(this._weatherCityRepository);

  @override
  Future<Either<ErrorsModel, WeatherInfoModel>> call(
          WeatherCityParams params) async =>
      _weatherCityRepository.getCurrentCityLocationWeather(params.toJson());
}
