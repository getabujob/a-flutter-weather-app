import '../../core/bloc/currentWeatherCubit/current_weather_cubit.dart';
import '../../core/bloc/currentWeatherCubit/data/current_location_params.dart';
import '../../core/di/di.dart';

class FindPlaceLocation {
  getData(double longitude, double latitude) async {
    return inject<CurrentWeatherCubit>()
      ..getStatements(
        WeatherParams(longitude: longitude, latitude: latitude),
      );
  }
}
