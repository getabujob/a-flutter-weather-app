import 'package:flutter/widgets.dart';
import 'package:weather_app/shared/configs/size_config.dart';

void setUpScreenUtil(BuildContext context) {
  SizeConfig().init(context);
}
