import 'package:flutter_test/flutter_test.dart'; 
import 'package:weather_app/core/network/endpoints.dart';
 

 void main() { 
 
 test('Testing weather endpoints', () {
    String _url =
        'https://api.openweathermap.org/data/2.5/weather/';

         expect(EndPoints.getCurrentWeather.url, _url);
  });
  
}

 